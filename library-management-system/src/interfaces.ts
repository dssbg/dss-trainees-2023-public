// interfaces.ts

export interface Book {
  id: number;
  title: string;
  author: string;
  publicationDate: Date;
}

export interface User {
  id: number;
  name: string;
  email: string;
  passwordHash: string;
}

export interface BookReservation {
  id: number;
  bookId: number;
  userId: number;
  reservationDate: Date;
}
