import React from 'react';

import BookList from './components/BookList';
import BookDetails from './components/BookDetails';
import './styles/main.css';

const App: React.FC = () => {
  return (
    <div className="container">
      <div className="navbar">Library Management System</div>
      <div className="book-list">
        <BookList />
      </div>
      <div className="book-details">
        <BookDetails />
      </div>
      <div className="footer">© 2023 Library Management System. All rights reserved.</div>
    </div>
  );
};
export default App;